FROM docker.io/alpine:latest

RUN apk update
RUN apk add openjdk8-jre libarchive-tools curl bash

RUN mkdir -p /opt/test-amapj && curl -sSLf https://amapj.fr/dist/amapj-dist-V034.zip | bsdtar -C /opt/test-amapj/ -xvf-

RUN echo -n 'export JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk/jre' > /opt/test-amapj/apache-tomcat/bin/setenv.sh
RUN chmod 755 /opt/test-amapj/apache-tomcat/bin/*.sh

RUN printf '/bin/sh\ncd /opt/test-amapj/apache-tomcat/bin/\nsh startup.sh\nsleep 1\ntail -f /opt/test-amapj/apache-tomcat/logs/catalina.out' > /root/entrypoint.sh
RUN chmod +x /root/entrypoint.sh

ENTRYPOINT sh /root/entrypoint.sh